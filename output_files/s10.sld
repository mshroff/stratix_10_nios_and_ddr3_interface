<sld_project_info>
  <sld_infos>
    <sld_info hpath="auto_fab_0" name="auto_fab_0">
      <assignment_values>
        <assignment_value text="QSYS_NAME alt_sld_fab_0 HAS_SOPCINFO 1 GENERATION_ID 0 ENTITY_NAME alt_sld_fab SLD_FAB 1 DESIGN_HASH 7cd3d6e87beb61b420e7"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="desg_instance|timer_0" library="system_timer_0" name="timer_0">
      <assignment_values>
        <assignment_value text="QSYS_NAME system_timer_0 HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="desg_instance|sysid_qsys_0" library="system_sysid_qsys_0" name="sysid_qsys_0">
      <assignment_values>
        <assignment_value text="QSYS_NAME system_sysid_qsys_0 HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="desg_instance|reset_in" library="system_reset_in" name="reset_in">
      <assignment_values>
        <assignment_value text="QSYS_NAME system_reset_in HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="desg_instance|reset_done" library="system_pio_0" name="reset_done">
      <assignment_values>
        <assignment_value text="QSYS_NAME system_pio_0 HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="desg_instance|reset_133" library="system_reset_bridge_0" name="reset_133">
      <assignment_values>
        <assignment_value text="QSYS_NAME system_reset_bridge_0 HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="desg_instance|onchip_memory2_0" library="system_onchip_memory2_0" name="onchip_memory2_0">
      <assignment_values>
        <assignment_value text="QSYS_NAME system_onchip_memory2_0 HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="desg_instance|nios2_gen2_0" library="system_nios2_gen2_0" name="nios2_gen2_0">
      <assignment_values>
        <assignment_value text="QSYS_NAME system_nios2_gen2_0 HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="desg_instance|local_reset_req" library="system_pio_3" name="local_reset_req">
      <assignment_values>
        <assignment_value text="QSYS_NAME system_pio_3 HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="desg_instance|jtag_uart_0" library="system_jtag_uart_0" name="jtag_uart_0">
      <assignment_values>
        <assignment_value text="QSYS_NAME system_jtag_uart_0 HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="desg_instance|emif_s10_0" library="system_emif_s10_0" name="emif_s10_0">
      <assignment_values>
        <assignment_value text="QSYS_NAME system_emif_s10_0 HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="desg_instance|clock_in" library="system_clock_in" name="clock_in">
      <assignment_values>
        <assignment_value text="QSYS_NAME system_clock_in HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="desg_instance|clk_133" library="system_clock_bridge_0" name="clk_133">
      <assignment_values>
        <assignment_value text="QSYS_NAME system_clock_bridge_0 HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="desg_instance|cal_success" library="system_pio_1" name="cal_success">
      <assignment_values>
        <assignment_value text="QSYS_NAME system_pio_1 HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="desg_instance|cal_fail" library="system_pio_2" name="cal_fail">
      <assignment_values>
        <assignment_value text="QSYS_NAME system_pio_2 HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
    <sld_info hpath="desg_instance" library="system" name="desg_instance">
      <assignment_values>
        <assignment_value text="QSYS_NAME system HAS_SOPCINFO 1 GENERATION_ID 0"/>
      </assignment_values>
    </sld_info>
  </sld_infos>
</sld_project_info>
