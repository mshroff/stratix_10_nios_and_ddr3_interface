// --------------------------------------------------------------------
// Copyright (c) 2013 by Terasic Technologies Inc.
// --------------------------------------------------------------------
//
// Permission:
//
//   Terasic grants permission to use and modify this code for use
//   in synthesis for all Terasic Development Boards and Altera Development
//   Kits made by Terasic.  Other use of this code, including the selling
//   ,duplication, or modification of any portion is strictly prohibited.
//
// Disclaimer:
//
//   This VHDL/Verilog or C/C++ source code is intended as a design reference
//   which illustrates how these types of functions can be implemented.
//   It is the user's responsibility to verify their design for
//   consistency and functionality through the use of formal
//   verification methods.  Terasic provides no warranty regarding the use
//   or functionality of this code.
//
// --------------------------------------------------------------------
//
//                     Terasic Technologies Inc
//                     E. Rd Sec. 1. JhuBei City,
//                     HsinChu County, Taiwan
//                     302
//
//                     web: http://www.terasic.com/
//                     email: support@terasic.com
//
// --------------------------------------------------------------------

#include "terasic_includes.h"
#include "mem_verify.h"

//#define KEY_BASE 0x40041030
//#define DDR3_STATUS_BASE 0x40041020

int main()
{
	bool bPassA;
		bool bPassSubA[4];
		int TestIndex ;
		alt_u32 InitValue = 0x01;
		bool bShowMessage = TRUE, bTimeout, bDone;
		alt_u32 TimeStart, TimeCalStart, TimeElapsed;
		alt_u16 cal_success_status,reset_done_status,cal_fail_status;
		const alt_u8 ButtonMask = 0x03; // 2 key
		const alt_u32 CalTimeout = alt_ticks_per_second()*3/2; // 1.5 second
		const alt_u32 ResetTimeout = alt_ticks_per_second()*2; // 2 second
		alt_u64 span_base;
		alt_u32 span_base_lo, span_base_hi;
		alt_u32 i;

		printf("===== DDR3 Test! Size=%dMB (CPU Clock:%d) =====\r\n", EMIF_S10_0_SPAN/1024/1024, ALT_CPU_CPU_FREQ);

		IOWR(LOCAL_RESET_REQ_BASE, 0x00, 0x00);

	        printf("\n==========================================================\n");

	        bPassA = TRUE;

	        TestIndex = 0;
	        do{
			TestIndex++;
			printf("=====> DDR3 Testing, Iteration: %d\n", TestIndex);

			// reset (0 --> 1 --> 0)

			IOWR(LOCAL_RESET_REQ_BASE, 0x00, 0x01);
			usleep(2*1000);
			IOWR(LOCAL_RESET_REQ_BASE, 0x00, 0x00);
			usleep(2*1000);

			TimeStart = alt_nticks();
			bTimeout = FALSE;
			bDone = FALSE;

			// check reset done
				reset_done_status =  IORD(RESET_DONE_BASE,  0x00);
				if(reset_done_status)
					bDone = TRUE;
				else if ((alt_nticks()-TimeStart) > ResetTimeout)
					bTimeout = TRUE;
			}while(!bDone && !bTimeout);

			if (!bTimeout){
				TimeElapsed = alt_nticks()-TimeStart;
				printf("DDR3x4 Reset durations, %.3f seconds\r\n", (float)TimeElapsed/(float)alt_ticks_per_second());
			}else{
				printf("DDR3x4 Reset Timeout, \r\n");
				printf("status: Register Value = %04x, \r\n", reset_done_status);
			}

			// check calibration done
			if (!bTimeout){
				bDone = FALSE;

				//usleep(500*1000);

				TimeCalStart = alt_nticks();
				do{
					cal_success_status =  IORD(CAL_SUCCESS_BASE,  0x00);
					cal_fail_status    =  IORD(CAL_FAIL_BASE,  0x00);
					if (cal_success_status && (!cal_fail_status))
						bDone = TRUE;
					else if ((alt_nticks()-TimeCalStart) > CalTimeout)
						bTimeout = TRUE;
				}while(!bDone && !bTimeout);

				if (bTimeout){
					printf("DDR3x4 Calibration Timeout, \r\n");
					printf("status: Cal_Success Register Value ( = %04x, \r\n", cal_success_status);
					printf("status: Cal_Fail Register Value ( = %04x, \r\n", cal_fail_status);
				}
				else{
					printf("DDR3 Calibration Duration:%.3f seconds, \r\n", (float)(alt_nticks()-TimeCalStart)/(float)alt_ticks_per_second());
				}

			}
			////////////////////////////
					// Testing //
					// DDR3-A
					TimeStart = alt_nticks();
					printf("== DDR3-A Testing...\r\n");
					if ( (!cal_success_status && cal_fail_status) || (!reset_done_status) ){
						printf("Failed due to Calibration/Reset timeout\r\n");
						bPassA = FALSE;
					}else{
						//printf("status=%xh\r\n", Status);
					}

					if (bPassA)
						bPassA = TMEM_Verify(EMIF_S10_0_BASE, EMIF_S10_0_SPAN, InitValue,  bShowMessage);

        	TimeElapsed = alt_nticks() - TimeStart;
        	printf("DDR3 test:%s, %d seconds\r\n", bPassA?"Pass":"NG", (int)(TimeElapsed/alt_ticks_per_second()));


            usleep(800*1000);
// while(1)


    return 0;
}


