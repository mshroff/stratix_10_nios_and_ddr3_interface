# Stratix 10 Nios Platform with DDR3 interface


## Description

This project sets up Stratix 10 FPGA L-Tile (1SG280LU2F50E2VGS3) to communicate with a DDR3 x72 HiLo memory daughter card, using the EMIF Interface controlled by a NIOS II processor.

The project also includes a Memory Test Software code that could be downloaded into the NIOS II processor. 

This project uses the [Startix 10 L-Tile Dev kit](https://www.intel.com/content/www/us/en/programmable/products/boards_and_kits/dev-kits/altera/kit-s10-fpga.html) and a [Micron
MT41K512M16TNA-107:E daughter card](https://www.micron.com/products/dram/ddr3-sdram/part-catalog/mt41k512m16tna-107).

## Acknowledgements
The memory test software has been adapted by Example designs provided by Terasic for the DE10-pro board
___
This project is still a Work in Progress

