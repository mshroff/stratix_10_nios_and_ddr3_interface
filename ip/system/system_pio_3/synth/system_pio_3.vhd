-- system_pio_3.vhd

-- Generated using ACDS version 18.0 219

library IEEE;
library altera_avalon_pio_180;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity system_pio_3 is
	port (
		clk        : in  std_logic                     := '0';             --                 clk.clk
		out_port   : out std_logic;                                        -- external_connection.export
		reset_n    : in  std_logic                     := '0';             --               reset.reset_n
		address    : in  std_logic_vector(1 downto 0)  := (others => '0'); --                  s1.address
		write_n    : in  std_logic                     := '0';             --                    .write_n
		writedata  : in  std_logic_vector(31 downto 0) := (others => '0'); --                    .writedata
		chipselect : in  std_logic                     := '0';             --                    .chipselect
		readdata   : out std_logic_vector(31 downto 0)                     --                    .readdata
	);
end entity system_pio_3;

architecture rtl of system_pio_3 is
	component system_pio_3_altera_avalon_pio_180_grr62iq_cmp is
		port (
			clk        : in  std_logic                     := 'X';             -- clk
			reset_n    : in  std_logic                     := 'X';             -- reset_n
			address    : in  std_logic_vector(1 downto 0)  := (others => 'X'); -- address
			write_n    : in  std_logic                     := 'X';             -- write_n
			writedata  : in  std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
			chipselect : in  std_logic                     := 'X';             -- chipselect
			readdata   : out std_logic_vector(31 downto 0);                    -- readdata
			out_port   : out std_logic                                         -- export
		);
	end component system_pio_3_altera_avalon_pio_180_grr62iq_cmp;

	for pio_3 : system_pio_3_altera_avalon_pio_180_grr62iq_cmp
		use entity altera_avalon_pio_180.system_pio_3_altera_avalon_pio_180_grr62iq;
begin

	pio_3 : component system_pio_3_altera_avalon_pio_180_grr62iq_cmp
		port map (
			clk        => clk,        --                 clk.clk
			reset_n    => reset_n,    --               reset.reset_n
			address    => address,    --                  s1.address
			write_n    => write_n,    --                    .write_n
			writedata  => writedata,  --                    .writedata
			chipselect => chipselect, --                    .chipselect
			readdata   => readdata,   --                    .readdata
			out_port   => out_port    -- external_connection.export
		);

end architecture rtl; -- of system_pio_3
