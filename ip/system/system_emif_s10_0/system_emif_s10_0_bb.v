module system_emif_s10_0 (
		output wire         amm_ready_0,               //                ctrl_amm_0.waitrequest_n
		input  wire         amm_read_0,                //                          .read
		input  wire         amm_write_0,               //                          .write
		input  wire [24:0]  amm_address_0,             //                          .address
		output wire [511:0] amm_readdata_0,            //                          .readdata
		input  wire [511:0] amm_writedata_0,           //                          .writedata
		input  wire [6:0]   amm_burstcount_0,          //                          .burstcount
		input  wire [63:0]  amm_byteenable_0,          //                          .byteenable
		output wire         amm_readdatavalid_0,       //                          .readdatavalid
		output wire         ctrl_ecc_user_interrupt_0, // ctrl_ecc_user_interrupt_0.ctrl_ecc_user_interrupt
		output wire         emif_usr_clk,              //              emif_usr_clk.clk
		output wire         emif_usr_reset_n,          //          emif_usr_reset_n.reset_n
		input  wire         local_reset_req,           //           local_reset_req.local_reset_req
		output wire         local_reset_done,          //        local_reset_status.local_reset_done
		output wire [0:0]   mem_ck,                    //                       mem.mem_ck
		output wire [0:0]   mem_ck_n,                  //                          .mem_ck_n
		output wire [14:0]  mem_a,                     //                          .mem_a
		output wire [2:0]   mem_ba,                    //                          .mem_ba
		output wire [0:0]   mem_cke,                   //                          .mem_cke
		output wire [0:0]   mem_cs_n,                  //                          .mem_cs_n
		output wire [0:0]   mem_odt,                   //                          .mem_odt
		output wire [0:0]   mem_reset_n,               //                          .mem_reset_n
		output wire [0:0]   mem_we_n,                  //                          .mem_we_n
		output wire [0:0]   mem_ras_n,                 //                          .mem_ras_n
		output wire [0:0]   mem_cas_n,                 //                          .mem_cas_n
		inout  wire [8:0]   mem_dqs,                   //                          .mem_dqs
		inout  wire [8:0]   mem_dqs_n,                 //                          .mem_dqs_n
		inout  wire [71:0]  mem_dq,                    //                          .mem_dq
		output wire [8:0]   mem_dm,                    //                          .mem_dm
		input  wire         oct_rzqin,                 //                       oct.oct_rzqin
		output wire         pll_locked,                //                pll_locked.pll_locked
		input  wire         pll_ref_clk,               //               pll_ref_clk.clk
		output wire         local_cal_success,         //                    status.local_cal_success
		output wire         local_cal_fail             //                          .local_cal_fail
	);
endmodule

