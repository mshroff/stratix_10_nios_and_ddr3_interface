// This file is dynamically generated and is for information purposes only.
// It is not used during compilation or simulation.

SEQ_GPT
   0x5C00   SEQ_GPT_GLOBAL_PAR_VER        : 2          0x00000002
   0x5C04   SEQ_GPT_NIOS_C_VER            : 1          0x00000001
   0x5C08   SEQ_GPT_COLUMN_ID             : 1          0x00000001
   0x5C0C   SEQ_GPT_NUM_IOPACKS           : 14         0x0000000E
   0x5C10   SEQ_GPT_NIOS_CLK_FREQ_KHZ     : 2000000    0x001E8480
   0x5C14   SEQ_GPT_PARAM_TABLE_SIZE      : 364        0x0000016C
   0x5C18   SEQ_GPT_GLOBAL_SKIP_STEPS     : 31         0x0000001F
   0x5C1C   SEQ_GPT_GLOBAL_CAL_CONFIG     : 0          0x00000000
   0x5C20   SEQ_GPT_SLAVE_CLK_DIVIDER     : 30         0x0000001E
   0x5C24   SEQ_GPT_INTERFACE_PAR_PTRS    
   0x5C24      static_array_elem [0]      : 92         0x0000005C
   0x5C28      static_array_elem [1]      : 0          0x00000000
   0x5C2C      static_array_elem [2]      : 0          0x00000000
   0x5C30      static_array_elem [3]      : 0          0x00000000
   0x5C34      static_array_elem [4]      : 0          0x00000000
   0x5C38      static_array_elem [5]      : 0          0x00000000
   0x5C3C      static_array_elem [6]      : 0          0x00000000
   0x5C40      static_array_elem [7]      : 0          0x00000000
   0x5C44      static_array_elem [8]      : 0          0x00000000
   0x5C48      static_array_elem [9]      : 0          0x00000000
   0x5C4C      static_array_elem [10]     : 0          0x00000000
   0x5C50      static_array_elem [11]     : 0          0x00000000
   0x5C54      static_array_elem [12]     : 0          0x00000000
   0x5C58      static_array_elem [13]     : 0          0x00000000
SEQ_PT
   0x5C5C   SEQ_PT_IP_VER                 : 18432      0x4800    
   0x5C5E   SEQ_PT_INTERFACE_PAR_VER      : 2          0x0002    
   0x5C60   SEQ_PT_DEBUG_DATA_PTR         : 0          0x0000    
   0x5C62   SEQ_PT_USER_COMMAND_PTR       : 0          0x0000    
   0x5C64   SEQ_PT_MEMORY_TYPE            : 0          0x00      
   0x5C65   SEQ_PT_DIMM_TYPE              : 0          0x00      
   0x5C66   SEQ_PT_CONTROLLER_TYPE        : 0          0x00      
   0x5C67   SEQ_PT_RESERVED               : 0          0x00      
   0x5C68   SEQ_PT_AFI_CLK_FREQ_KHZ       : 533334     0x00082356
   0x5C6C   SEQ_PT_BURST_LEN              : 8          0x08      
   0x5C6D   SEQ_PT_READ_LATENCY           : 14         0x0E      
   0x5C6E   SEQ_PT_WRITE_LATENCY          : 10         0x0A      
   0x5C6F   SEQ_PT_NUM_RANKS              : 1          0x01      
   0x5C70   SEQ_PT_NUM_DIMMS              : 1          0x01      
   0x5C71   SEQ_PT_NUM_DQS_WR             : 9          0x09      
   0x5C72   SEQ_PT_NUM_DQS_RD             : 9          0x09      
   0x5C73   SEQ_PT_NUM_DQ                 : 72         0x48      
   0x5C74   SEQ_PT_NUM_DM                 : 9          0x09      
   0x5C75   SEQ_PT_ADDR_WIDTH             : 15         0x0F      
   0x5C76   SEQ_PT_BANK_WIDTH             : 3          0x03      
   0x5C77   SEQ_PT_CS_WIDTH               : 1          0x01      
   0x5C78   SEQ_PT_CKE_WIDTH              : 1          0x01      
   0x5C79   SEQ_PT_ODT_WIDTH              : 1          0x01      
   0x5C7A   SEQ_PT_C_WIDTH                : 0          0x00      
   0x5C7B   SEQ_PT_BANK_GROUP_WIDTH       : 0          0x00      
   0x5C7C   SEQ_PT_ADDR_MIRROR            : 0          0x00      
   0x5C7D   SEQ_PT_CK_WIDTH               : 1          0x01      
   0x5C7E   SEQ_PT_CAL_DATA_SIZE          : 16         0x10      
   0x5C7F   SEQ_PT_NUM_LRDIMM_CFG         : 0          0x00      
   0x5C80   SEQ_PT_NUM_AC_ROM_ENUMS       : 48         0x30      
   0x5C81   SEQ_PT_NUM_CENTERS            : 3          0x03      
   0x5C82   SEQ_PT_NUM_CA_LANES           : 3          0x03      
   0x5C83   SEQ_PT_NUM_DATA_LANES         : 9          0x09      
   0x5C84   SEQ_PT_ODT_TABLE_LO           : 4          0x00000004
   0x5C88   SEQ_PT_ODT_TABLE_HI           : 0          0x00000000
   0x5C8C   SEQ_PT_CAL_CONFIG             : 220258433  0x0D20E081
   0x5C90   SEQ_PT_DBG_CONFIG             : 0          0x0000    
   0x5C92   SEQ_PT_CAL_DATA_PTR           : 168        0x00A8    
   0x00A8      CAL_TREFI                  : 7800       0x00001E78 7800
   0x00AC      CAL_TRFC                   : 260        0x00000104 260
   0x00B0      CAL_ADDR0                  : 0          0x00000000 0
   0x00B4      CAL_ADDR1                  : 8          0x00000008 8
   0x5C94   SEQ_PT_DBG_SKIP_RANKS         : 0          0x00000000
   0x5C98   SEQ_PT_DBG_SKIP_GROUPS        : 0          0x00000000
   0x5C9C   SEQ_PT_DBG_SKIP_STEPS         : 1561856    0x0017D500
   0x5CA0   SEQ_PT_NUM_MR                 : 4          0x04      
   0x5CA1   SEQ_PT_NUM_DIMM_MR            : 4          0x04      
   0x5CA2   SEQ_PT_TILE_ID_PTR            : 184        0x00B8    
   0x00B8      TILE [0]                   : 12         0x0C       (T) = (1)
   0x00B9      TILE [1]                   : 4          0x04       (T) = (0)
   0x00BA      TILE [2]                   : 20         0x14       (T) = (2)
   0x00BB      AC_LANE [0]                : 8          0x08       (T L) = (1 0)
   0x00BC      AC_LANE [1]                : 9          0x09       (T L) = (1 1)
   0x00BD      AC_LANE [2]                : 10         0x0A       (T L) = (1 2)
   0x00BE      DATA_LANE [0]              : 11         0x0B       (T L) = (1 3)
   0x00BF      DATA_LANE [1]              : 0          0x00       (T L) = (0 0)
   0x00C0      DATA_LANE [2]              : 1          0x01       (T L) = (0 1)
   0x00C1      DATA_LANE [3]              : 2          0x02       (T L) = (0 2)
   0x00C2      DATA_LANE [4]              : 3          0x03       (T L) = (0 3)
   0x00C3      DATA_LANE [5]              : 16         0x10       (T L) = (2 0)
   0x00C4      DATA_LANE [6]              : 17         0x11       (T L) = (2 1)
   0x00C5      DATA_LANE [7]              : 18         0x12       (T L) = (2 2)
   0x00C6      DATA_LANE [8]              : 19         0x13       (T L) = (2 3)
   0x00C7      ALIGN_PAD                  : 0          0x00       
   0x5CA4   SEQ_PT_PIN_ADDR_PTR           : 200        0x00C8    
   0x00C8      PORT_MEM_CKE [0]           : 6          0x06       (T L P) = (1 0 6 )  AC_LANE [0]
   0x00C9                                 : 0          0x00       UNUSED_AC_PORT
   0x00CA                                 : 0          0x00       UNUSED_AC_PORT
   0x00CB                                 : 0          0x00       UNUSED_AC_PORT
   0x00CC      PORT_MEM_ODT [0]           : 4          0x04       (T L P) = (1 0 4 )  AC_LANE [0]
   0x00CD                                 : 0          0x00       UNUSED_AC_PORT
   0x00CE                                 : 0          0x00       UNUSED_AC_PORT
   0x00CF                                 : 0          0x00       UNUSED_AC_PORT
   0x00D0      PORT_MEM_RESET_N [0]       : 1          0x01       (T L P) = (1 0 1 )  AC_LANE [0]
   0x00D1      PORT_MEM_WE_N [0]          : 0          0x00       (T L P) = (1 0 0 )  AC_LANE [0]
   0x00D2      PORT_MEM_CAS_N [0]         : 40         0x28       (T L P) = (1 2 8 )  AC_LANE [2]
   0x00D3      PORT_MEM_RAS_N [0]         : 39         0x27       (T L P) = (1 2 7 )  AC_LANE [2]
   0x00D4      PORT_MEM_CS_N [0]          : 2          0x02       (T L P) = (1 0 2 )  AC_LANE [0]
   0x00D5                                 : 0          0x00       UNUSED_AC_PORT
   0x00D6                                 : 0          0x00       UNUSED_AC_PORT
   0x00D7                                 : 0          0x00       UNUSED_AC_PORT
   0x00D8      PORT_MEM_BA [0]            : 41         0x29       (T L P) = (1 2 9 )  AC_LANE [2]
   0x00D9      PORT_MEM_BA [1]            : 42         0x2A       (T L P) = (1 2 10)  AC_LANE [2]
   0x00DA      PORT_MEM_BA [2]            : 43         0x2B       (T L P) = (1 2 11)  AC_LANE [2]
   0x00DB      PORT_MEM_A [0]             : 16         0x10       (T L P) = (1 1 0 )  AC_LANE [1]
   0x00DC      PORT_MEM_A [1]             : 17         0x11       (T L P) = (1 1 1 )  AC_LANE [1]
   0x00DD      PORT_MEM_A [2]             : 18         0x12       (T L P) = (1 1 2 )  AC_LANE [1]
   0x00DE      PORT_MEM_A [3]             : 19         0x13       (T L P) = (1 1 3 )  AC_LANE [1]
   0x00DF      PORT_MEM_A [4]             : 20         0x14       (T L P) = (1 1 4 )  AC_LANE [1]
   0x00E0      PORT_MEM_A [5]             : 21         0x15       (T L P) = (1 1 5 )  AC_LANE [1]
   0x00E1      PORT_MEM_A [6]             : 22         0x16       (T L P) = (1 1 6 )  AC_LANE [1]
   0x00E2      PORT_MEM_A [7]             : 23         0x17       (T L P) = (1 1 7 )  AC_LANE [1]
   0x00E3      PORT_MEM_A [8]             : 24         0x18       (T L P) = (1 1 8 )  AC_LANE [1]
   0x00E4      PORT_MEM_A [9]             : 25         0x19       (T L P) = (1 1 9 )  AC_LANE [1]
   0x00E5      PORT_MEM_A [10]            : 26         0x1A       (T L P) = (1 1 10)  AC_LANE [1]
   0x00E6      PORT_MEM_A [11]            : 27         0x1B       (T L P) = (1 1 11)  AC_LANE [1]
   0x00E7      PORT_MEM_A [12]            : 35         0x23       (T L P) = (1 2 3 )  AC_LANE [2]
   0x00E8      PORT_MEM_A [13]            : 36         0x24       (T L P) = (1 2 4 )  AC_LANE [2]
   0x00E9      PORT_MEM_A [14]            : 37         0x25       (T L P) = (1 2 5 )  AC_LANE [2]
   0x00EA                                 : 0          0x00       UNUSED_AC_PORT
   0x00EB                                 : 0          0x00       UNUSED_AC_PORT
   0x00EC                                 : 0          0x00       UNUSED_AC_PORT
   0x00ED                                 : 0          0x00       UNUSED_AC_PORT
   0x00EE                                 : 0          0x00       UNUSED_AC_PORT
   0x00EF                                 : 0          0x00       UNUSED_AC_PORT
   0x00F0      PORT_MEM_CK [0]            : 8          0x08       (T L P) = (1 0 8 )  AC_LANE [0]
   0x00F1      PORT_MEM_CK_N [0]          : 9          0x09       (T L P) = (1 0 9 )  AC_LANE [0]
   0x00F2                                 : 0          0x00       UNUSED_AC_PORT
   0x00F3                                 : 0          0x00       UNUSED_AC_PORT
   0x00F4                                 : 0          0x00       UNUSED_AC_PORT
   0x00F5                                 : 0          0x00       UNUSED_AC_PORT
   0x00F6                                 : 0          0x00       UNUSED_AC_PORT
   0x00F7                                 : 0          0x00       UNUSED_AC_PORT
   0x00F8      PORT_MEM_DQS [0]           : 20         0x14       (T L P) = (0 0 4 )  DATA_LANE [1]
   0x00F9      PORT_MEM_DQS [1]           : 36         0x24       (T L P) = (0 1 4 )  DATA_LANE [2]
   0x00FA      PORT_MEM_DQS [2]           : 52         0x34       (T L P) = (0 2 4 )  DATA_LANE [3]
   0x00FB      PORT_MEM_DQS [3]           : 68         0x44       (T L P) = (0 3 4 )  DATA_LANE [4]
   0x00FC      PORT_MEM_DQS [4]           : 4          0x04       (T L P) = (1 3 4 )  DATA_LANE [0]
   0x00FD      PORT_MEM_DQS [5]           : 84         0x54       (T L P) = (2 0 4 )  DATA_LANE [5]
   0x00FE      PORT_MEM_DQS [6]           : 100        0x64       (T L P) = (2 1 4 )  DATA_LANE [6]
   0x00FF      PORT_MEM_DQS [7]           : 116        0x74       (T L P) = (2 2 4 )  DATA_LANE [7]
   0x0100      PORT_MEM_DQS [8]           : 132        0x84       (T L P) = (2 3 4 )  DATA_LANE [8]
   0x0101      PORT_MEM_DQS_N [0]         : 21         0x15       (T L P) = (0 0 5 )  DATA_LANE [1]
   0x0102      PORT_MEM_DQS_N [1]         : 37         0x25       (T L P) = (0 1 5 )  DATA_LANE [2]
   0x0103      PORT_MEM_DQS_N [2]         : 53         0x35       (T L P) = (0 2 5 )  DATA_LANE [3]
   0x0104      PORT_MEM_DQS_N [3]         : 69         0x45       (T L P) = (0 3 5 )  DATA_LANE [4]
   0x0105      PORT_MEM_DQS_N [4]         : 5          0x05       (T L P) = (1 3 5 )  DATA_LANE [0]
   0x0106      PORT_MEM_DQS_N [5]         : 85         0x55       (T L P) = (2 0 5 )  DATA_LANE [5]
   0x0107      PORT_MEM_DQS_N [6]         : 101        0x65       (T L P) = (2 1 5 )  DATA_LANE [6]
   0x0108      PORT_MEM_DQS_N [7]         : 117        0x75       (T L P) = (2 2 5 )  DATA_LANE [7]
   0x0109      PORT_MEM_DQS_N [8]         : 133        0x85       (T L P) = (2 3 5 )  DATA_LANE [8]
   0x010A      PORT_MEM_DM [0]            : 27         0x1B       (T L P) = (0 0 11)  DATA_LANE [1]
   0x010B      PORT_MEM_DM [1]            : 43         0x2B       (T L P) = (0 1 11)  DATA_LANE [2]
   0x010C      PORT_MEM_DM [2]            : 59         0x3B       (T L P) = (0 2 11)  DATA_LANE [3]
   0x010D      PORT_MEM_DM [3]            : 75         0x4B       (T L P) = (0 3 11)  DATA_LANE [4]
   0x010E      PORT_MEM_DM [4]            : 11         0x0B       (T L P) = (1 3 11)  DATA_LANE [0]
   0x010F      PORT_MEM_DM [5]            : 91         0x5B       (T L P) = (2 0 11)  DATA_LANE [5]
   0x0110      PORT_MEM_DM [6]            : 107        0x6B       (T L P) = (2 1 11)  DATA_LANE [6]
   0x0111      PORT_MEM_DM [7]            : 123        0x7B       (T L P) = (2 2 11)  DATA_LANE [7]
   0x0112      PORT_MEM_DM [8]            : 139        0x8B       (T L P) = (2 3 11)  DATA_LANE [8]
   0x0113      PORT_MEM_DQ [0]            : 17         0x11       (T L P) = (0 0 1 )  DATA_LANE [1]
   0x0114      PORT_MEM_DQ [1]            : 18         0x12       (T L P) = (0 0 2 )  DATA_LANE [1]
   0x0115      PORT_MEM_DQ [2]            : 19         0x13       (T L P) = (0 0 3 )  DATA_LANE [1]
   0x0116      PORT_MEM_DQ [3]            : 22         0x16       (T L P) = (0 0 6 )  DATA_LANE [1]
   0x0117      PORT_MEM_DQ [4]            : 23         0x17       (T L P) = (0 0 7 )  DATA_LANE [1]
   0x0118      PORT_MEM_DQ [5]            : 24         0x18       (T L P) = (0 0 8 )  DATA_LANE [1]
   0x0119      PORT_MEM_DQ [6]            : 25         0x19       (T L P) = (0 0 9 )  DATA_LANE [1]
   0x011A      PORT_MEM_DQ [7]            : 26         0x1A       (T L P) = (0 0 10)  DATA_LANE [1]
   0x011B      PORT_MEM_DQ [8]            : 33         0x21       (T L P) = (0 1 1 )  DATA_LANE [2]
   0x011C      PORT_MEM_DQ [9]            : 34         0x22       (T L P) = (0 1 2 )  DATA_LANE [2]
   0x011D      PORT_MEM_DQ [10]           : 35         0x23       (T L P) = (0 1 3 )  DATA_LANE [2]
   0x011E      PORT_MEM_DQ [11]           : 38         0x26       (T L P) = (0 1 6 )  DATA_LANE [2]
   0x011F      PORT_MEM_DQ [12]           : 39         0x27       (T L P) = (0 1 7 )  DATA_LANE [2]
   0x0120      PORT_MEM_DQ [13]           : 40         0x28       (T L P) = (0 1 8 )  DATA_LANE [2]
   0x0121      PORT_MEM_DQ [14]           : 41         0x29       (T L P) = (0 1 9 )  DATA_LANE [2]
   0x0122      PORT_MEM_DQ [15]           : 42         0x2A       (T L P) = (0 1 10)  DATA_LANE [2]
   0x0123      PORT_MEM_DQ [16]           : 49         0x31       (T L P) = (0 2 1 )  DATA_LANE [3]
   0x0124      PORT_MEM_DQ [17]           : 50         0x32       (T L P) = (0 2 2 )  DATA_LANE [3]
   0x0125      PORT_MEM_DQ [18]           : 51         0x33       (T L P) = (0 2 3 )  DATA_LANE [3]
   0x0126      PORT_MEM_DQ [19]           : 54         0x36       (T L P) = (0 2 6 )  DATA_LANE [3]
   0x0127      PORT_MEM_DQ [20]           : 55         0x37       (T L P) = (0 2 7 )  DATA_LANE [3]
   0x0128      PORT_MEM_DQ [21]           : 56         0x38       (T L P) = (0 2 8 )  DATA_LANE [3]
   0x0129      PORT_MEM_DQ [22]           : 57         0x39       (T L P) = (0 2 9 )  DATA_LANE [3]
   0x012A      PORT_MEM_DQ [23]           : 58         0x3A       (T L P) = (0 2 10)  DATA_LANE [3]
   0x012B      PORT_MEM_DQ [24]           : 65         0x41       (T L P) = (0 3 1 )  DATA_LANE [4]
   0x012C      PORT_MEM_DQ [25]           : 66         0x42       (T L P) = (0 3 2 )  DATA_LANE [4]
   0x012D      PORT_MEM_DQ [26]           : 67         0x43       (T L P) = (0 3 3 )  DATA_LANE [4]
   0x012E      PORT_MEM_DQ [27]           : 70         0x46       (T L P) = (0 3 6 )  DATA_LANE [4]
   0x012F      PORT_MEM_DQ [28]           : 71         0x47       (T L P) = (0 3 7 )  DATA_LANE [4]
   0x0130      PORT_MEM_DQ [29]           : 72         0x48       (T L P) = (0 3 8 )  DATA_LANE [4]
   0x0131      PORT_MEM_DQ [30]           : 73         0x49       (T L P) = (0 3 9 )  DATA_LANE [4]
   0x0132      PORT_MEM_DQ [31]           : 74         0x4A       (T L P) = (0 3 10)  DATA_LANE [4]
   0x0133      PORT_MEM_DQ [32]           : 1          0x01       (T L P) = (1 3 1 )  DATA_LANE [0]
   0x0134      PORT_MEM_DQ [33]           : 2          0x02       (T L P) = (1 3 2 )  DATA_LANE [0]
   0x0135      PORT_MEM_DQ [34]           : 3          0x03       (T L P) = (1 3 3 )  DATA_LANE [0]
   0x0136      PORT_MEM_DQ [35]           : 6          0x06       (T L P) = (1 3 6 )  DATA_LANE [0]
   0x0137      PORT_MEM_DQ [36]           : 7          0x07       (T L P) = (1 3 7 )  DATA_LANE [0]
   0x0138      PORT_MEM_DQ [37]           : 8          0x08       (T L P) = (1 3 8 )  DATA_LANE [0]
   0x0139      PORT_MEM_DQ [38]           : 9          0x09       (T L P) = (1 3 9 )  DATA_LANE [0]
   0x013A      PORT_MEM_DQ [39]           : 10         0x0A       (T L P) = (1 3 10)  DATA_LANE [0]
   0x013B      PORT_MEM_DQ [40]           : 81         0x51       (T L P) = (2 0 1 )  DATA_LANE [5]
   0x013C      PORT_MEM_DQ [41]           : 82         0x52       (T L P) = (2 0 2 )  DATA_LANE [5]
   0x013D      PORT_MEM_DQ [42]           : 83         0x53       (T L P) = (2 0 3 )  DATA_LANE [5]
   0x013E      PORT_MEM_DQ [43]           : 86         0x56       (T L P) = (2 0 6 )  DATA_LANE [5]
   0x013F      PORT_MEM_DQ [44]           : 87         0x57       (T L P) = (2 0 7 )  DATA_LANE [5]
   0x0140      PORT_MEM_DQ [45]           : 88         0x58       (T L P) = (2 0 8 )  DATA_LANE [5]
   0x0141      PORT_MEM_DQ [46]           : 89         0x59       (T L P) = (2 0 9 )  DATA_LANE [5]
   0x0142      PORT_MEM_DQ [47]           : 90         0x5A       (T L P) = (2 0 10)  DATA_LANE [5]
   0x0143      PORT_MEM_DQ [48]           : 97         0x61       (T L P) = (2 1 1 )  DATA_LANE [6]
   0x0144      PORT_MEM_DQ [49]           : 98         0x62       (T L P) = (2 1 2 )  DATA_LANE [6]
   0x0145      PORT_MEM_DQ [50]           : 99         0x63       (T L P) = (2 1 3 )  DATA_LANE [6]
   0x0146      PORT_MEM_DQ [51]           : 102        0x66       (T L P) = (2 1 6 )  DATA_LANE [6]
   0x0147      PORT_MEM_DQ [52]           : 103        0x67       (T L P) = (2 1 7 )  DATA_LANE [6]
   0x0148      PORT_MEM_DQ [53]           : 104        0x68       (T L P) = (2 1 8 )  DATA_LANE [6]
   0x0149      PORT_MEM_DQ [54]           : 105        0x69       (T L P) = (2 1 9 )  DATA_LANE [6]
   0x014A      PORT_MEM_DQ [55]           : 106        0x6A       (T L P) = (2 1 10)  DATA_LANE [6]
   0x014B      PORT_MEM_DQ [56]           : 113        0x71       (T L P) = (2 2 1 )  DATA_LANE [7]
   0x014C      PORT_MEM_DQ [57]           : 114        0x72       (T L P) = (2 2 2 )  DATA_LANE [7]
   0x014D      PORT_MEM_DQ [58]           : 115        0x73       (T L P) = (2 2 3 )  DATA_LANE [7]
   0x014E      PORT_MEM_DQ [59]           : 118        0x76       (T L P) = (2 2 6 )  DATA_LANE [7]
   0x014F      PORT_MEM_DQ [60]           : 119        0x77       (T L P) = (2 2 7 )  DATA_LANE [7]
   0x0150      PORT_MEM_DQ [61]           : 120        0x78       (T L P) = (2 2 8 )  DATA_LANE [7]
   0x0151      PORT_MEM_DQ [62]           : 121        0x79       (T L P) = (2 2 9 )  DATA_LANE [7]
   0x0152      PORT_MEM_DQ [63]           : 122        0x7A       (T L P) = (2 2 10)  DATA_LANE [7]
   0x0153      PORT_MEM_DQ [64]           : 129        0x81       (T L P) = (2 3 1 )  DATA_LANE [8]
   0x0154      PORT_MEM_DQ [65]           : 130        0x82       (T L P) = (2 3 2 )  DATA_LANE [8]
   0x0155      PORT_MEM_DQ [66]           : 131        0x83       (T L P) = (2 3 3 )  DATA_LANE [8]
   0x0156      PORT_MEM_DQ [67]           : 134        0x86       (T L P) = (2 3 6 )  DATA_LANE [8]
   0x0157      PORT_MEM_DQ [68]           : 135        0x87       (T L P) = (2 3 7 )  DATA_LANE [8]
   0x0158      PORT_MEM_DQ [69]           : 136        0x88       (T L P) = (2 3 8 )  DATA_LANE [8]
   0x0159      PORT_MEM_DQ [70]           : 137        0x89       (T L P) = (2 3 9 )  DATA_LANE [8]
   0x015A      PORT_MEM_DQ [71]           : 138        0x8A       (T L P) = (2 3 10)  DATA_LANE [8]
   0x015B      ALIGN_PAD                  : 0          0x00       
   0x5CA6   SEQ_PT_MR_PTR                 : 348        0x015C    
   0x015C      MR0                        : 36         0x00000024 00000000000000100100
   0x0160      MR1                        : 65538      0x00010002 00010000000000000010
   0x0164      MR2                        : 132136     0x00020428 00100000010000101000
   0x0168      MR3                        : 196608     0x00030000 00110000000000000000
