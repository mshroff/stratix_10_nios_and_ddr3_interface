// (C) 2001-2018 Intel Corporation. All rights reserved.
// Your use of Intel Corporation's design tools, logic functions and other 
// software and tools, and its AMPP partner logic functions, and any output 
// files from any of the foregoing (including device programming or simulation 
// files), and any associated documentation or information are expressly subject 
// to the terms and conditions of the Intel Program License Subscription 
// Agreement, Intel FPGA IP License Agreement, or other applicable 
// license agreement, including, without limitation, that your use is for the 
// sole purpose of programming logic devices manufactured by Intel and sold by 
// Intel or its authorized distributors.  Please refer to the applicable 
// agreement for further details.


///////////////////////////////////////////////////////////////////////////////
//  Reset request sequencing state machine.
//
///////////////////////////////////////////////////////////////////////////////
module altera_emif_arch_nd_local_reset # (
   parameter PHY_CONFIG_ENUM    = "",
   parameter IS_HPS             = 0
) (
   input  logic   afi_clk,
   input  logic   afi_reset_n,
   input  logic   emif_usr_clk,
   input  logic   emif_usr_reset_n,

   input  logic   local_reset_req_int,
   output logic   core2seq_reset_req,

   output logic   local_reset_done,
   input  logic   seq2core_reset_done
);
   timeunit 1ns;
   timeprecision 1ps;

   generate
      if (IS_HPS) begin: hps
         assign core2seq_reset_req = 1'b0;
      end else begin : non_hps
         logic clk;
         logic reset_n;

         if (PHY_CONFIG_ENUM == "CONFIG_PHY_AND_HARD_CTRL") begin : hmc
            assign clk = emif_usr_clk;
            assign reset_n = emif_usr_reset_n;
         end else begin : non_hmc
            assign clk = afi_clk;
            assign reset_n = afi_reset_n;
         end

         enum {
            WAIT_RESET_DONE,
            WAIT_USER_RESET_REQ_1ST_DEASSERT,
            WAIT_USER_RESET_REQ_ASSERT,
            WAIT_USER_RESET_REQ_2ND_DEASSERT,
            ASSERT_CORE2SEQ_RESET_REQ
         } state;

         ////////////////////////////////////////////////////////////////////
         // State machine
         ////////////////////////////////////////////////////////////////////
         always_ff @(posedge clk, negedge reset_n)
         begin
            if (!reset_n) begin
               state <= WAIT_RESET_DONE;
            end else begin
               case (state)
                  WAIT_RESET_DONE:
                     // Wait until sequencer signals it's ready to accept a reset request.
                     if (seq2core_reset_done)
                        state <= (local_reset_req_int ? WAIT_USER_RESET_REQ_1ST_DEASSERT : WAIT_USER_RESET_REQ_ASSERT);
                  WAIT_USER_RESET_REQ_1ST_DEASSERT:
                     if (~local_reset_req_int)
                        state <= WAIT_USER_RESET_REQ_ASSERT;
                  WAIT_USER_RESET_REQ_ASSERT:
                     if (local_reset_req_int)
                        state <= WAIT_USER_RESET_REQ_2ND_DEASSERT;
                  WAIT_USER_RESET_REQ_2ND_DEASSERT:
                     if (~local_reset_req_int)
                        state <= ASSERT_CORE2SEQ_RESET_REQ;
                  ASSERT_CORE2SEQ_RESET_REQ:
                     state <= ASSERT_CORE2SEQ_RESET_REQ;
                  default:
                     state <= WAIT_RESET_DONE;
               endcase
            end
         end

         ////////////////////////////////////////////////////////////////////
         // Output generation
         ////////////////////////////////////////////////////////////////////
         assign core2seq_reset_req = (state == ASSERT_CORE2SEQ_RESET_REQ) ? 1'b1 : 1'b0;

         // Instead of passing seq2core_reset_done directly to user, we have the ability
         // to acknowledge the reset request earlier, as soon as a local_reset_req pulse
         // is detected.
         assign local_reset_done = (state == WAIT_USER_RESET_REQ_1ST_DEASSERT ||
                                    state == WAIT_USER_RESET_REQ_ASSERT ||
                                    state == WAIT_USER_RESET_REQ_2ND_DEASSERT) ? 1'b1 : 1'b0;
      end
   endgenerate
endmodule
