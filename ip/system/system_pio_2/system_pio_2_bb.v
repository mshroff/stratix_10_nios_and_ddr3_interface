module system_pio_2 (
		input  wire        clk,      //                 clk.clk
		input  wire        in_port,  // external_connection.export
		input  wire        reset_n,  //               reset.reset_n
		input  wire [1:0]  address,  //                  s1.address
		output wire [31:0] readdata  //                    .readdata
	);
endmodule

