module s10 (
		output wire [0:0]  emif_s10_0_mem_mem_ck,               //         emif_s10_0_mem.emif_s10_0_mem_mem_ck
		output wire [0:0]  emif_s10_0_mem_mem_ck_n,             //                       .emif_s10_0_mem_mem_ck_n
		output wire [14:0] emif_s10_0_mem_mem_a,                //                       .emif_s10_0_mem_mem_a
		output wire [2:0]  emif_s10_0_mem_mem_ba,               //                       .emif_s10_0_mem_mem_ba
		output wire [0:0]  emif_s10_0_mem_mem_cke,              //                       .emif_s10_0_mem_mem_cke
		output wire [0:0]  emif_s10_0_mem_mem_cs_n,             //                       .emif_s10_0_mem_mem_cs_n
		output wire [0:0]  emif_s10_0_mem_mem_odt,              //                       .emif_s10_0_mem_mem_odt
		output wire [0:0]  emif_s10_0_mem_mem_reset_n,          //                       .emif_s10_0_mem_mem_reset_n
		output wire [0:0]  emif_s10_0_mem_mem_we_n,             //                       .emif_s10_0_mem_mem_we_n
		output wire [0:0]  emif_s10_0_mem_mem_ras_n,            //                       .emif_s10_0_mem_mem_ras_n
		output wire [0:0]  emif_s10_0_mem_mem_cas_n,            //                       .emif_s10_0_mem_mem_cas_n
		inout  wire [8:0]  emif_s10_0_mem_mem_dqs,              //                       .emif_s10_0_mem_mem_dqs
		inout  wire [8:0]  emif_s10_0_mem_mem_dqs_n,            //                       .emif_s10_0_mem_mem_dqs_n
		inout  wire [71:0] emif_s10_0_mem_mem_dq,               //                       .emif_s10_0_mem_mem_dq
		output wire [8:0]  emif_s10_0_mem_mem_dm,               //                       .emif_s10_0_mem_mem_dm
		input  wire        emif_s10_0_oct_oct_rzqin,            //         emif_s10_0_oct.oct_rzqin
		output wire        emif_s10_0_status_local_cal_success_LED, //      emif_s10_0_status.local_cal_success
		output wire        emif_s10_0_status_local_cal_fail_LED,    //                       .local_cal_fail
		input  wire        local_reset_req,                     //        local_reset_req.local_reset_req
		output wire        local_reset_done_LED,                     //     local_reset_status.local_reset_done
		output wire			 local_reset_not_done_LED,
		input wire			 reset_button,
		input wire 			CLK50,
		input wire 			CLK133
	);


		
		wire ddr3_local_reset_done;
		wire ddr3_status_local_cal_fail;
		wire ddr3_status_local_cal_success;
		
		wire ddr3_local_reset_req;

		
		assign ddr3_status_array = {ddr3_status_local_cal_fail, ddr3_status_local_cal_success, ddr3_local_reset_done};
		
		
		 system desg_instance(
		.emif_s10_0_mem_mem_ck              (emif_s10_0_mem_mem_ck),                                      //  output,    width = 1,                mem.emif_s10_0_mem_mem_ck
		.emif_s10_0_mem_mem_ck_n            (emif_s10_0_mem_mem_ck_n),                                    //  output,    width = 1,                   .emif_s10_0_mem_mem_ck_n
		.emif_s10_0_mem_mem_a               (emif_s10_0_mem_mem_a),                                       //  output,   width = 15,                   .emif_s10_0_mem_mem_a
		.emif_s10_0_mem_mem_ba              (emif_s10_0_mem_mem_ba),                                      //  output,    width = 3,                   .emif_s10_0_mem_mem_ba
		.emif_s10_0_mem_mem_cke             (emif_s10_0_mem_mem_cke),                                     //  output,    width = 1,                   .emif_s10_0_mem_mem_cke
		.emif_s10_0_mem_mem_cs_n            (emif_s10_0_mem_mem_cs_n),                                    //  output,    width = 1,                   .emif_s10_0_mem_mem_cs_n
		.emif_s10_0_mem_mem_odt             (emif_s10_0_mem_mem_odt),                                     //  output,    width = 1,                   .emif_s10_0_mem_mem_odt
		.emif_s10_0_mem_mem_reset_n         (emif_s10_0_mem_mem_reset_n),                                 //  output,    width = 1,                   .emif_s10_0_mem_mem_reset_n
		.emif_s10_0_mem_mem_we_n            (emif_s10_0_mem_mem_we_n),                                    //  output,    width = 1,                   .emif_s10_0_mem_mem_we_n
		.emif_s10_0_mem_mem_ras_n           (emif_s10_0_mem_mem_ras_n),                                   //  output,    width = 1,                   .emif_s10_0_mem_mem_ras_n
		.emif_s10_0_mem_mem_cas_n           (emif_s10_0_mem_mem_cas_n),                                   //  output,    width = 1,                   .emif_s10_0_mem_mem_cas_n
		.emif_s10_0_mem_mem_dqs             (emif_s10_0_mem_mem_dqs),                                     //   inout,    width = 9,                   .emif_s10_0_mem_mem_dqs
		.emif_s10_0_mem_mem_dqs_n           (emif_s10_0_mem_mem_dqs_n),                                   //   inout,    width = 9,                   .emif_s10_0_mem_mem_dqs_n
		.emif_s10_0_mem_mem_dq              (emif_s10_0_mem_mem_dq),                                      //   inout,   width = 72,                   .emif_s10_0_mem_mem_dq
		.emif_s10_0_mem_mem_dm              (emif_s10_0_mem_mem_dm),                                      //  output,    width = 9,                   .emif_s10_0_mem_mem_dm
		.emif_s10_0_oct_oct_rzqin           (emif_s10_0_oct_oct_rzqin),                                   //   input,    width = 1,                oct.oct_rzqin

		.emif_s10_0_status_local_cal_success   (ddr3_status_local_cal_success),                        //  output,    width = 1,             status.local_cal_success
		.emif_s10_0_status_local_cal_fail      (ddr3_status_local_cal_fail),
		.clk_clk									(CLK50),
		.clk_133_clk							(CLK133),
		.emif_s10_0_local_reset_req_local_reset_req	 (ddr3_local_reset_req),
		.emif_s10_0_local_reset_status_local_reset_done (ddr3_local_reset_done),
		
		.cal_success_external_connection_export		(ddr3_status_local_cal_success),
		.cal_fail_external_connection_export			(ddr3_status_local_cal_fail),
		.reset_done_external_connection_export			(ddr3_local_reset_done),
		.local_reset_req_external_connection_export		(ddr3_local_reset_req),
		.reset_reset											 (~reset_button)													// must be inverse so NIOS processor can reset
																																			// i.e. press button ->0. We want intialization to be at reset =1'b0.  
		);
		
	assign 	emif_s10_0_status_local_cal_success_LED  = ~ddr3_status_local_cal_success;
	assign 	 emif_s10_0_status_local_cal_fail_LED = ~ddr3_status_local_cal_fail ;
	assign	 local_reset_done_LED = (ddr3_local_reset_done)? 1'b0 : 1'b1;
	assign 	local_reset_not_done_LED = (!ddr3_local_reset_done) ? 1'b0 : 1'b1;
	
	endmodule
	