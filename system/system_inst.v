	system u0 (
		.cal_fail_external_connection_export            (_connected_to_cal_fail_external_connection_export_),            //   input,   width = 1,        cal_fail_external_connection.export
		.cal_success_external_connection_export         (_connected_to_cal_success_external_connection_export_),         //   input,   width = 1,     cal_success_external_connection.export
		.clk_clk                                        (_connected_to_clk_clk_),                                        //   input,   width = 1,                                 clk.clk
		.clk_133_clk                                    (_connected_to_clk_133_clk_),                                    //   input,   width = 1,                             clk_133.clk
		.emif_s10_0_local_reset_req_local_reset_req     (_connected_to_emif_s10_0_local_reset_req_local_reset_req_),     //   input,   width = 1,          emif_s10_0_local_reset_req.local_reset_req
		.emif_s10_0_local_reset_status_local_reset_done (_connected_to_emif_s10_0_local_reset_status_local_reset_done_), //  output,   width = 1,       emif_s10_0_local_reset_status.local_reset_done
		.emif_s10_0_mem_mem_ck                          (_connected_to_emif_s10_0_mem_mem_ck_),                          //  output,   width = 1,                      emif_s10_0_mem.mem_ck
		.emif_s10_0_mem_mem_ck_n                        (_connected_to_emif_s10_0_mem_mem_ck_n_),                        //  output,   width = 1,                                    .mem_ck_n
		.emif_s10_0_mem_mem_a                           (_connected_to_emif_s10_0_mem_mem_a_),                           //  output,  width = 15,                                    .mem_a
		.emif_s10_0_mem_mem_ba                          (_connected_to_emif_s10_0_mem_mem_ba_),                          //  output,   width = 3,                                    .mem_ba
		.emif_s10_0_mem_mem_cke                         (_connected_to_emif_s10_0_mem_mem_cke_),                         //  output,   width = 1,                                    .mem_cke
		.emif_s10_0_mem_mem_cs_n                        (_connected_to_emif_s10_0_mem_mem_cs_n_),                        //  output,   width = 1,                                    .mem_cs_n
		.emif_s10_0_mem_mem_odt                         (_connected_to_emif_s10_0_mem_mem_odt_),                         //  output,   width = 1,                                    .mem_odt
		.emif_s10_0_mem_mem_reset_n                     (_connected_to_emif_s10_0_mem_mem_reset_n_),                     //  output,   width = 1,                                    .mem_reset_n
		.emif_s10_0_mem_mem_we_n                        (_connected_to_emif_s10_0_mem_mem_we_n_),                        //  output,   width = 1,                                    .mem_we_n
		.emif_s10_0_mem_mem_ras_n                       (_connected_to_emif_s10_0_mem_mem_ras_n_),                       //  output,   width = 1,                                    .mem_ras_n
		.emif_s10_0_mem_mem_cas_n                       (_connected_to_emif_s10_0_mem_mem_cas_n_),                       //  output,   width = 1,                                    .mem_cas_n
		.emif_s10_0_mem_mem_dqs                         (_connected_to_emif_s10_0_mem_mem_dqs_),                         //   inout,   width = 9,                                    .mem_dqs
		.emif_s10_0_mem_mem_dqs_n                       (_connected_to_emif_s10_0_mem_mem_dqs_n_),                       //   inout,   width = 9,                                    .mem_dqs_n
		.emif_s10_0_mem_mem_dq                          (_connected_to_emif_s10_0_mem_mem_dq_),                          //   inout,  width = 72,                                    .mem_dq
		.emif_s10_0_mem_mem_dm                          (_connected_to_emif_s10_0_mem_mem_dm_),                          //  output,   width = 9,                                    .mem_dm
		.emif_s10_0_oct_oct_rzqin                       (_connected_to_emif_s10_0_oct_oct_rzqin_),                       //   input,   width = 1,                      emif_s10_0_oct.oct_rzqin
		.emif_s10_0_status_local_cal_success            (_connected_to_emif_s10_0_status_local_cal_success_),            //  output,   width = 1,                   emif_s10_0_status.local_cal_success
		.emif_s10_0_status_local_cal_fail               (_connected_to_emif_s10_0_status_local_cal_fail_),               //  output,   width = 1,                                    .local_cal_fail
		.local_reset_req_external_connection_export     (_connected_to_local_reset_req_external_connection_export_),     //  output,   width = 1, local_reset_req_external_connection.export
		.reset_reset                                    (_connected_to_reset_reset_),                                    //   input,   width = 1,                               reset.reset
		.reset_done_external_connection_export          (_connected_to_reset_done_external_connection_export_)           //   input,   width = 1,      reset_done_external_connection.export
	);

