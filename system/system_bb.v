module system (
		input  wire        cal_fail_external_connection_export,            //        cal_fail_external_connection.export
		input  wire        cal_success_external_connection_export,         //     cal_success_external_connection.export
		input  wire        clk_clk,                                        //                                 clk.clk
		input  wire        clk_133_clk,                                    //                             clk_133.clk
		input  wire        emif_s10_0_local_reset_req_local_reset_req,     //          emif_s10_0_local_reset_req.local_reset_req
		output wire        emif_s10_0_local_reset_status_local_reset_done, //       emif_s10_0_local_reset_status.local_reset_done
		output wire [0:0]  emif_s10_0_mem_mem_ck,                          //                      emif_s10_0_mem.mem_ck
		output wire [0:0]  emif_s10_0_mem_mem_ck_n,                        //                                    .mem_ck_n
		output wire [14:0] emif_s10_0_mem_mem_a,                           //                                    .mem_a
		output wire [2:0]  emif_s10_0_mem_mem_ba,                          //                                    .mem_ba
		output wire [0:0]  emif_s10_0_mem_mem_cke,                         //                                    .mem_cke
		output wire [0:0]  emif_s10_0_mem_mem_cs_n,                        //                                    .mem_cs_n
		output wire [0:0]  emif_s10_0_mem_mem_odt,                         //                                    .mem_odt
		output wire [0:0]  emif_s10_0_mem_mem_reset_n,                     //                                    .mem_reset_n
		output wire [0:0]  emif_s10_0_mem_mem_we_n,                        //                                    .mem_we_n
		output wire [0:0]  emif_s10_0_mem_mem_ras_n,                       //                                    .mem_ras_n
		output wire [0:0]  emif_s10_0_mem_mem_cas_n,                       //                                    .mem_cas_n
		inout  wire [8:0]  emif_s10_0_mem_mem_dqs,                         //                                    .mem_dqs
		inout  wire [8:0]  emif_s10_0_mem_mem_dqs_n,                       //                                    .mem_dqs_n
		inout  wire [71:0] emif_s10_0_mem_mem_dq,                          //                                    .mem_dq
		output wire [8:0]  emif_s10_0_mem_mem_dm,                          //                                    .mem_dm
		input  wire        emif_s10_0_oct_oct_rzqin,                       //                      emif_s10_0_oct.oct_rzqin
		output wire        emif_s10_0_status_local_cal_success,            //                   emif_s10_0_status.local_cal_success
		output wire        emif_s10_0_status_local_cal_fail,               //                                    .local_cal_fail
		output wire        local_reset_req_external_connection_export,     // local_reset_req_external_connection.export
		input  wire        reset_reset,                                    //                               reset.reset
		input  wire        reset_done_external_connection_export           //      reset_done_external_connection.export
	);
endmodule

