	component system is
		port (
			cal_fail_external_connection_export            : in    std_logic                     := 'X';             -- export
			cal_success_external_connection_export         : in    std_logic                     := 'X';             -- export
			clk_clk                                        : in    std_logic                     := 'X';             -- clk
			clk_133_clk                                    : in    std_logic                     := 'X';             -- clk
			emif_s10_0_local_reset_req_local_reset_req     : in    std_logic                     := 'X';             -- local_reset_req
			emif_s10_0_local_reset_status_local_reset_done : out   std_logic;                                        -- local_reset_done
			emif_s10_0_mem_mem_ck                          : out   std_logic_vector(0 downto 0);                     -- mem_ck
			emif_s10_0_mem_mem_ck_n                        : out   std_logic_vector(0 downto 0);                     -- mem_ck_n
			emif_s10_0_mem_mem_a                           : out   std_logic_vector(14 downto 0);                    -- mem_a
			emif_s10_0_mem_mem_ba                          : out   std_logic_vector(2 downto 0);                     -- mem_ba
			emif_s10_0_mem_mem_cke                         : out   std_logic_vector(0 downto 0);                     -- mem_cke
			emif_s10_0_mem_mem_cs_n                        : out   std_logic_vector(0 downto 0);                     -- mem_cs_n
			emif_s10_0_mem_mem_odt                         : out   std_logic_vector(0 downto 0);                     -- mem_odt
			emif_s10_0_mem_mem_reset_n                     : out   std_logic_vector(0 downto 0);                     -- mem_reset_n
			emif_s10_0_mem_mem_we_n                        : out   std_logic_vector(0 downto 0);                     -- mem_we_n
			emif_s10_0_mem_mem_ras_n                       : out   std_logic_vector(0 downto 0);                     -- mem_ras_n
			emif_s10_0_mem_mem_cas_n                       : out   std_logic_vector(0 downto 0);                     -- mem_cas_n
			emif_s10_0_mem_mem_dqs                         : inout std_logic_vector(8 downto 0)  := (others => 'X'); -- mem_dqs
			emif_s10_0_mem_mem_dqs_n                       : inout std_logic_vector(8 downto 0)  := (others => 'X'); -- mem_dqs_n
			emif_s10_0_mem_mem_dq                          : inout std_logic_vector(71 downto 0) := (others => 'X'); -- mem_dq
			emif_s10_0_mem_mem_dm                          : out   std_logic_vector(8 downto 0);                     -- mem_dm
			emif_s10_0_oct_oct_rzqin                       : in    std_logic                     := 'X';             -- oct_rzqin
			emif_s10_0_status_local_cal_success            : out   std_logic;                                        -- local_cal_success
			emif_s10_0_status_local_cal_fail               : out   std_logic;                                        -- local_cal_fail
			local_reset_req_external_connection_export     : out   std_logic;                                        -- export
			reset_reset                                    : in    std_logic                     := 'X';             -- reset
			reset_done_external_connection_export          : in    std_logic                     := 'X'              -- export
		);
	end component system;

	u0 : component system
		port map (
			cal_fail_external_connection_export            => CONNECTED_TO_cal_fail_external_connection_export,            --        cal_fail_external_connection.export
			cal_success_external_connection_export         => CONNECTED_TO_cal_success_external_connection_export,         --     cal_success_external_connection.export
			clk_clk                                        => CONNECTED_TO_clk_clk,                                        --                                 clk.clk
			clk_133_clk                                    => CONNECTED_TO_clk_133_clk,                                    --                             clk_133.clk
			emif_s10_0_local_reset_req_local_reset_req     => CONNECTED_TO_emif_s10_0_local_reset_req_local_reset_req,     --          emif_s10_0_local_reset_req.local_reset_req
			emif_s10_0_local_reset_status_local_reset_done => CONNECTED_TO_emif_s10_0_local_reset_status_local_reset_done, --       emif_s10_0_local_reset_status.local_reset_done
			emif_s10_0_mem_mem_ck                          => CONNECTED_TO_emif_s10_0_mem_mem_ck,                          --                      emif_s10_0_mem.mem_ck
			emif_s10_0_mem_mem_ck_n                        => CONNECTED_TO_emif_s10_0_mem_mem_ck_n,                        --                                    .mem_ck_n
			emif_s10_0_mem_mem_a                           => CONNECTED_TO_emif_s10_0_mem_mem_a,                           --                                    .mem_a
			emif_s10_0_mem_mem_ba                          => CONNECTED_TO_emif_s10_0_mem_mem_ba,                          --                                    .mem_ba
			emif_s10_0_mem_mem_cke                         => CONNECTED_TO_emif_s10_0_mem_mem_cke,                         --                                    .mem_cke
			emif_s10_0_mem_mem_cs_n                        => CONNECTED_TO_emif_s10_0_mem_mem_cs_n,                        --                                    .mem_cs_n
			emif_s10_0_mem_mem_odt                         => CONNECTED_TO_emif_s10_0_mem_mem_odt,                         --                                    .mem_odt
			emif_s10_0_mem_mem_reset_n                     => CONNECTED_TO_emif_s10_0_mem_mem_reset_n,                     --                                    .mem_reset_n
			emif_s10_0_mem_mem_we_n                        => CONNECTED_TO_emif_s10_0_mem_mem_we_n,                        --                                    .mem_we_n
			emif_s10_0_mem_mem_ras_n                       => CONNECTED_TO_emif_s10_0_mem_mem_ras_n,                       --                                    .mem_ras_n
			emif_s10_0_mem_mem_cas_n                       => CONNECTED_TO_emif_s10_0_mem_mem_cas_n,                       --                                    .mem_cas_n
			emif_s10_0_mem_mem_dqs                         => CONNECTED_TO_emif_s10_0_mem_mem_dqs,                         --                                    .mem_dqs
			emif_s10_0_mem_mem_dqs_n                       => CONNECTED_TO_emif_s10_0_mem_mem_dqs_n,                       --                                    .mem_dqs_n
			emif_s10_0_mem_mem_dq                          => CONNECTED_TO_emif_s10_0_mem_mem_dq,                          --                                    .mem_dq
			emif_s10_0_mem_mem_dm                          => CONNECTED_TO_emif_s10_0_mem_mem_dm,                          --                                    .mem_dm
			emif_s10_0_oct_oct_rzqin                       => CONNECTED_TO_emif_s10_0_oct_oct_rzqin,                       --                      emif_s10_0_oct.oct_rzqin
			emif_s10_0_status_local_cal_success            => CONNECTED_TO_emif_s10_0_status_local_cal_success,            --                   emif_s10_0_status.local_cal_success
			emif_s10_0_status_local_cal_fail               => CONNECTED_TO_emif_s10_0_status_local_cal_fail,               --                                    .local_cal_fail
			local_reset_req_external_connection_export     => CONNECTED_TO_local_reset_req_external_connection_export,     -- local_reset_req_external_connection.export
			reset_reset                                    => CONNECTED_TO_reset_reset,                                    --                               reset.reset
			reset_done_external_connection_export          => CONNECTED_TO_reset_done_external_connection_export           --      reset_done_external_connection.export
		);

